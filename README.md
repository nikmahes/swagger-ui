
# README #

This swagger-ui is powered by express nodeserver
### What is this repository for? ###

* To host interactive swagger documentation of you API endpoints

### How do I get set up? ###

* git clone https://nikmahes@bitbucket.org/nikmahes/swagger-ui.git
* cd swagger-ui
* npm install
* npm start
* Open the hosted application in Web Browser http://localhost:5000
 
### Contributor(s)###

* Nikhil Maheshwari \[http://www.nikhilmaheshwari.com/\]